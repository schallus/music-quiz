/**
 * FILE cookies.js
 **/

/* global $ Handlebars */

'use strict';

/**
 *
 * Function which create a cookie
 * @param {String} cName Cookie name
 * @param {String|Object} cValue Cookie value (string or json)
 * @param {Numeric} exSeconds Expiration time in seconds
 *
 **/
function setCookie(cName, cValue, exSeconds) {
    if(cValue !== null && typeof cValue === 'object') {
        cValue = JSON.stringify(cValue);
    }

    let d = new Date();
    d.setTime(d.getTime() + (exSeconds*1000));
    let expires = 'expires='+ d.toUTCString();
    document.cookie = cName + '=' + cValue + ';' + expires + ';path=/';
}

/**
 *
 * Function which get a cookie
 * @param {String} cName Cookie name
 * @return {String|Object} Value of the cookie
 *
 **/
function getCookie(cName) {
    let name = cName + '=';
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            let cValue = c.substring(name.length, c.length);

            try {
                let json = JSON.parse(cValue);
                return json;
            } catch(e) {
                return cValue;
            }
        }
    }
    return '';
}

/**
 *
 * Function which check if a cookie exists. If yes, it calls the callback function
 * @param {String} cName Cookie name
 * @param {function(String)} callback
 *
 **/
function checkCookie(cName, callback) {
    let cookie = getCookie(cName);
    if (cookie != '') {
        callback(cookie);
    }
}

/**
 * FILE oauth.js
 **/

/**
 * GLOBAL VARIABLES
 **/

let ACCESS_TOKEN;
let USER_DATA;

/**
 * OAuth login request
 * @param {String} callback Callback function executed once we received the access_token.
 **/
function login(callback) {
	const CLIENT_ID = '6add326a80504a199a195132b566a766';
	const REDIRECT_URI = 'http://users.metropolia.fi/~floriasc/music-quiz/oauth.html';

	/**
     *
     * Create the login URL with the scopes given in as a parameter
     * @param {String[]} scopes Scopes necessary for the app.
     * @return {String} Login URL
     *
     **/
	function getLoginURL(scopes) {
		return 'https://accounts.spotify.com/authorize?client_id=' + CLIENT_ID +
            '&redirect_uri=' + encodeURIComponent(REDIRECT_URI) +
            '&scope=' + encodeURIComponent(scopes.join(' ')) +
            '&response_type=token';
	}

	let url = getLoginURL([
		'playlist-modify-private',
		'playlist-modify-public',
		'user-top-read',
		'user-read-email',
	]);

    // Event listener for POST message
	$(window).on('message onmessage', function(e) {
        let hash = JSON.parse(e.originalEvent.data);
        // If we received the access token, we call the callback function
        if (hash.type == 'access_token') {
			callback(hash.access_token);
		}
    });

    // Pop up window parameter
	let width = 450;
	let height = 730;
	let left = (screen.width / 2) - (width / 2);
	let top = (screen.height / 2) - (height / 2);

	// Open the Spotify connection window
	window.open(
        url,
        'Spotify',
        'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left
    );
}

/**
 *
 * Function which retrieves the user data
 * @param {String} accessToken User access_token needed for the query.
 * @return {Object} Object containing user data
 *
 **/
function getUserData(accessToken) {
    return $.ajax({
        url: 'https://api.spotify.com/v1/me',
        headers: {
           'Authorization': 'Bearer ' + accessToken,
        },
    });
}

/**
 *
 * Function which retrieves the playlists of the user
 * @param {String} accessToken User access_token needed for the query.
 * @return {Object} Object containing all the user playlists
 *
 **/
function getUserPlaylists(accessToken) {
    return $.ajax({
        url: 'https://api.spotify.com/v1/me/playlists',
        headers: {
           'Authorization': 'Bearer ' + accessToken,
        },
    });
}

function logout() {
    console.log('logout');
    setCookie('accessToken', null, 0);
    setCookie('user', null, 0);
    document.location.reload(true);
}

$(document).ready(function() {
    $('#spotifyAuthentication').click(function() {
        login(function(accessToken) {
            // Store the access token in your cookies
            setCookie('accessToken', accessToken, 3600);
            ACCESS_TOKEN = accessToken;

            // Get the user data
            getUserData(accessToken)
                .then(function(response) {
                    USER_DATA = response;
                    setCookie('user', response, 3600);
                    showUserProfile(response);
                });

            getUserPlaylists(accessToken)
                .then(function(response) {
                    showUserPlaylists(response);
                });
        });
    });
});

/**
 * FILE functions.js
 **/

/**
 *
 * Function which load a template
 * @param {String} templateName Name of the template to load
 * @param {function()} callback Callback function which will be called after loading the template
 * @param {Object} data Data to be compiled in the template
 *
 **/
function loadTemplate(templateName, callback, data) {
    data = data || {};

    let location = window.location.href;
    let directoryUrl = location.substring(0, location.lastIndexOf('/')+1);
    let templateUrl = directoryUrl+'templates/'+templateName+'.hbs';
    let page404Url = directoryUrl+'templates/404.hbs';

    $.ajax({
        url: templateUrl,
        type: 'HEAD',
        error: function() {
            // File not exists
            // 404 page

            $.get(page404Url, function(source) {
                let template=Handlebars.compile(source);

                let result = template(data);

                $('#wrapper').html(result);
            }, 'html');
        }, success: function() {
            // File exists
            // Load the template

            $.get(templateUrl, function(source) {
                let template=Handlebars.compile(source);

                let result = template(data);

                $('#wrapper').html(result);

                callback();
            }, 'html');
        },
    });
}

/**
 *
 * Shuffle the array given as a parameter
 * @param {Object[]} array Array to shuffle
 * @return {Object[]} Array shuffled
 *
 **/
function shuffleArray(array) {
    let currentIndex = array.length;
    let temporaryValue;
    let randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

/**
 * FILE geolocation.js
 **/

/**
 *
 * Check is the client browser is compatible with Geolocation API
 * @param {function(coords)} callback Function called after getting the current position
 *
 **/
function getCurrentLocation(callback) {
    navigator.geolocation.getCurrentPosition(function(position) {
        callback(position.coords);
    },
    function(failure) {
        $.getJSON('https://ipinfo.io/geo', function(response) {
            let loc = response.loc.split(',');
            let coords = {
                latitude: loc[0],
                longitude: loc[1],
            };
            callback(coords);
        });
    });
}

/**
 * FILE eventful.js
 **/

const eventfulAppKey = 'w7TJNSGwt59H7rBf';

/**
 *
 * Search for events around the location given as a parameter
 * @param {Object} location Object containing a geo location
 * @param {function(coords)} callback Function called after getting the events
 *
 **/
function eventSearch(location, callback) {
    $.ajax({
        url: 'http://api.eventful.com/json/events/search',
        type: 'GET',
        dataType: 'jsonp',
        data: {
            app_key: eventfulAppKey,
            location: location.latitude + ', ' + location.longitude,
            within: 100, // Radius in km
            units: 'km',
            date: 'Future',
            category: 'music',
            sort_order: 'date',
        },
        success: function(data) {
            callback(data);
        },
        error: function(httpReq, status, exception) {
            console.log(status + ' ' + exception);
        },
    });
}

/**
 * FILE handlebars.js
 **/

Handlebars.registerHelper('bold', function(options) {
    return new Handlebars.SafeString(
        '<strong>'
        + options.fn(this)
        + '</strong>');
});

Handlebars.registerHelper('formatDate', function(options) {
    let date = new Date(options.fn(this));
    return date.toDateString();
});

/**
 * FILE track.js
 **/

/**
 *
 * Track class
 *
 **/

function Track(track) {
    this.id = track.id;
    this.name = track.name;
    this.artistName = track.artists[0].name;
    this.artistId = track.artists[0].id;
    this.album = track.album.name;
    this.uri = track.uri;

    if(track.album.images.length > 1) {
        this.albumCover = track.album.images[1].url;
    } else {
        this.albumCover = null;
    }

    this.previewUrl = track.preview_url || null;
    this.audio;

    this.getId = function() {
        return this.id;
    };

    this.getName = function() {
        return this.name;
    };

    this.getArtist = function() {
        return this.artistName;
    };

    this.getArtistId = function() {
        return this.artistId;
    };

    this.getAlbum = function() {
        return this.album;
    };

     this.getUri = function() {
        return this.uri;
    };

    this.getAlbumCover = function() {
        return this.albumCover;
    };

    this.getPreviewUrl = function() {
        return this.previewUrl;
    };

    this.preload = function() {
        this.audio = new Audio(this.previewUrl);
        this.audio.load();
    };

    this.play = function() {
        if (this.audio == null) {
            this.preload();
        } else {
            this.audio.play();
        }
    };

    this.stop = function() {
        this.audio.pause();
        this.audio.currentTime = 0;
    };
}

/**
 * FILE main.js
 **/

/**
 * GLOBAL VARIABLES
 **/

let tracks = [];
let genre;
let nextTrack;
let nextTrackID = 0;
let score = 0;
let timer1;
let timer2;
let timer3;

/**
 *
 * Default duration of the quiz
 * Format : hh:mm:ss
 *
 **/
const quizDefaultDuration = '00:01:30';

/**
 *
 * Number of tracks to load when querying the Spotify Web API
 *
 **/
const limit = 200; // Multiple of 50 only

/**
 *
 * Country code used to search for the top tracks in this country
 *
 **/
const market = 'CH';

/**
 *
 * Function which add all the playlists given in parameter to the genre list
 * @param {Object[]} playlists Array of playlist
 *
 **/
function showUserPlaylists(playlists) {
    playlists.items.forEach(function(playlist) {
       addPlaylist(playlist);
    });
}

/**
 *
 * Function which add the playlist given in parameter to the genre list
 * @param {Object} playlist Playlist to add
 *
 **/
function addPlaylist(playlist) {
    let li = $('<li>');
    li.addClass('col-md-3 col-xs-6 genre playlist '+ playlist.id);

    let input = $('<input>');
    input.attr({
        type: 'radio',
        id: playlist.id,
        value: playlist.id,
        name: 'genre',
        owner: playlist.owner.id,
    });

    let label = $('<label>');
    label.attr({
        'for': playlist.id,
        'title': playlist.name,
    });

    let name;
    // If the playlist name is too long, we crop it to 16 character and add '...'
    if(playlist.name.length>16) {
        name = playlist.name.substring(0, 16)+'...';
    } else {
        name = playlist.name;
    }

    label.html(name);
    if(playlist.images.length>2) {
        label.css('background-image', 'url("' + playlist.images[2].url + '")');
    }

    li.append(input);
    li.append(label);

    $('#genre-selection ul').append(li);
}

/**
 *
 * This function display a link to the Spotify user profile
 * @param {Object} userProfile Object containing the Spotify user information
 *
 **/
function showUserProfile(userProfile) {
    // Hide the login button once connected
    $('#spotifyAuthentication').hide();

    let logoutButton = $('<button>');
    logoutButton.attr({
        id: 'myProfile',
        class: 'button user-button',
    });

    // If the user has full name, we display it, otherwise, we display his user id
    if(userProfile.display_name) {
        logoutButton.attr('title', 'Logout ' + userProfile.display_name);
    } else {
        logoutButton.attr('title', 'Logout ' + userProfile.id);
    }

    if(userProfile.images.length>0) {
        let profileImg = $('<img>');
        profileImg.attr({
            src: userProfile.images[0].url,
            alt: userProfile.display_name,
            id: 'profile_pic',
        });
        logoutButton.append(profileImg);
    }

    let userName = $('<span>');

    userName.html('Logout');
    logoutButton.append(userName);

    logoutButton.bind('click', logout);

    $('#main-header').append(logoutButton);
}

/**
 *
 * This function is called when the user press the button to start the quiz
 *
 **/
function startQuiz() {
    let data = {};
    data.genre = genre;
    loadTemplate('quiz', function() {
        // Start the countdown
        countdown();

        loadSimilarTrack(nextTrack);

        $('#quitQuiz').click(function() {
            endQuiz();
        });
    }, data);
}

/**
 *
 * This function is called when the user select an answer
 * @param {Object} answer Button pressed Object
 **/
function selectAnswer(answer) {
    // When a user select an answer, it stops the track currently playing
    nextTrack.stop();

    if(answer !== null) {
        if(answer.val()==nextTrack.getId()) {
            // If the answer is correct, it increases the score and add 3s to the timer
            score+=1;
            updateTimer(3000);
            // Animation when the score change
            $('#score').animate({
                fontSize: '4em',
            }, 200, function() {
                $(this)
                    .html(parseInt($('#score').html())+1)
                    .animate({
                        fontSize: '2em',
                    }, 200);
            });
            answer.css('outline-color', '#1ED760');
            setTimeout(function() {
                playNextTrack();
            }, 500);
        } else {
            // If the answer is wrong, we decrease the timer by 1s
            updateTimer(-1000);
            answer.css('outline-color', '#F44336');
            setTimeout(function() {
                playNextTrack();
            }, 250);
        }
    } else {
        playNextTrack();
    }

    function playNextTrack() {
        // If we reached the end of the table we shuffle it again and reset the index to 0
        if(nextTrackID<tracks.length-1) {
            nextTrackID++;
        } else {
            shuffleArray(tracks);
            nextTrackID = 0;
        }

        // Preload the next track
        nextTrack = tracks[nextTrackID];
        nextTrack.preload();

        loadSimilarTrack(nextTrack);
    }
}

/**
 *
 * This function display the answer (4 differents album cover)
 * @param {Object[]} similarTracks Array of tracks which are similar to the one playing
 *
 **/
function displayAnswers(similarTracks) {
    // Clear previous answers
    $('#quiz .questions').html('');

    let answers = similarTracks;
    answers.push(nextTrack);
    answers = shuffleArray(answers);

    // After 30seconds, if the user didn't select any answer, it passes to the next track
    timer2 = setTimeout(function() {
        selectAnswer(null);
    }, 30000);

    answers.forEach(function(answer) {
        let div = $('<div>');
        div.addClass('col-md-3 col-xs-6');

        let answerButton = $('<button>');
        answerButton.attr('value', answer.getId());
        answerButton.bind('click', function() {
            $('#quiz .questions button').attr('disabled', 'disabled');
            clearTimeout(timer2);
            selectAnswer($(this));
        });

        let img = $('<img>');
        img.attr('src', answer.getAlbumCover());
        answerButton.append(img);

        div.append(answerButton);

        $('#quiz .questions').append(div);
    });
    nextTrack.play();
}

/**
 *
 * Load similar track from Spotify API
 * @param {Object} track Track for which we want to search similar tracks
 *
 **/
function loadSimilarTrack(track) {
    // Number of tracks to load
    const LIMIT = 3;

    let similarTracks = [];

    $.ajax({
        url: 'https://api.spotify.com/v1/artists/' + track.getArtistId() + '/related-artists',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function(similarArtists) {
            for(let i = 0; i<LIMIT; i++) {
                let artist = similarArtists.artists[i];

                $.ajax({
                    url: 'https://api.spotify.com/v1/artists/' + artist.id + '/top-tracks',
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: {
                        country: market,
                    },
                    success: function(artistTopTracks) {
                        let randomTrackId = Math.floor(Math.random() * (artistTopTracks.tracks.length-1));
                        let topTrack = new Track(artistTopTracks.tracks[randomTrackId]);
                        similarTracks.push(topTrack);
                        if(similarTracks.length==LIMIT) {
                            displayAnswers(similarTracks);
                        }
                    },
                });
            }
        },
    });
}

/**
 *
 * Function called when the quiz is over
 * This function is loading the scoreboard, etc.
 *
 **/
function endQuiz() {
    $('#quiz .questions').hide();

    // Clear the timers which are still running
    if(timer1) {
        clearTimeout(timer1);
    }
    if(timer2) {
        clearTimeout(timer2);
    }

    nextTrack.stop();

    // Create an array with the tracks played
    let tracksPlayed = [];
    for(let i = 0; i < nextTrackID; i++) {
        tracksPlayed.push(tracks[i]);
    }

    let bestScore;

    // Save best score in cookies
    if(getCookie('bestScore') == '' || parseInt(getCookie('bestScore')) < score) {
        setCookie('bestScore', score, 2147483647); // 2038-01-19 04:14:07 = Maximum value we can set
        bestScore = score;
    } else {
        bestScore = parseInt(getCookie('bestScore'));
    }

    let data = {
        score: score,
        bestScore: bestScore,
        tracksPlayed: tracksPlayed,
        auth: ACCESS_TOKEN!==undefined, // return true if the user is authenticated with his Spotify account
    };

    /* getCurrentLocation(function(location) {
        eventSearch(location, function(result) {
            data.events = result.events; */

            // Load the scoreboard
            loadTemplate('scoreboard', function() {
                let audio;

                $('#tracksPlayed .addPlaylist').click(function() {
                    let trackUri = $(this).val();
                    addTrackToPlaylist(trackUri);
                });

                $('#tracksPlayed .playPreview').on('click', function() {
                    if(timer3) {
                        clearTimeout(timer3);
                    }

                    $("#tracksPlayed .playPreview").not(this).html('Play');
                    if(audio) {
                        audio.stop();
                    }

                    if($(this).html() == 'Play') {
                        let trackIndex = $(this).val();
                        playTrack(trackIndex);
                        $(this).html('Stop');
                    } else {
                        $(this).html('Play');
                    }

                    function playTrack(trackIndex) {
                        audio = tracks[trackIndex];
                        audio.play();

                        timer3 = setTimeout(function() {
                            $(this).html('Play');
                            audio = undefined;
                        }, 30000);
                    }
                });

                $('#restartQuiz').click(function() {
                    if(audio) {
                        audio.stop();
                    }

                    // Shuffle the tracks and reset the score
                    shuffleArray(tracks);
                    score = 0;
                    nextTrackID = 0;
                    startQuiz();
                });
            }, data);
        /* });
    }); */
}

/**
 *
 * This function add the track given in parameter to the 'Music Quiz' playlist
 * @param {String|String[]} trackUri Tracks uri
 *
 **/
function addTrackToPlaylist(trackUri) {
    const playlistName = 'Music Quiz';

    createPlaylist(playlistName, true, false, function(playlist) {
        console.log(playlist);

        let trackUris = [];
        if(Array.isArray(trackUri)) {
            trackUris = trackUri;
        } else {
            trackUris.push(trackUri);
        }

        // Add the track to the playlist
        let data = {
            'uris': trackUris,
        }

        data = JSON.stringify(data);

        $.ajax({
            url: 'https://api.spotify.com/v1/users/' + USER_DATA.id + '/playlists/' + playlist.id + '/tracks',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
            'Authorization': 'Bearer ' + ACCESS_TOKEN,
            'Content-Type': 'application/json',
            },
            data: data,
            success: function(data, textStatus, xhr) {
                if(xhr.status == 201) {
                    alert('Track successfully added to the playlist');
                } else {
                    alert('An error occured while adding the tracks to the playlist');
                }
            },
            error: function() {
                alert('An error occured while adding the tracks to the playlist');
            },
        });
    });
}

/**
 *
 * Create a Spotify playlist
 * @param {String} playlistName Name of the playlist
 * @param {Boolean} playlistPublic If true the playlist will be public
 * @param {Boolean} playlistCollaborative If true the playlist will be collaborative
 * @param {function(Object)} callback Function executed once the playlist created
 *
 **/
function createPlaylist(playlistName, playlistPublic, playlistCollaborative, callback) {
    // Check if the playlist exist already
    $.ajax({
        url: 'https://api.spotify.com/v1/me/playlists',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
           'Authorization': 'Bearer ' + ACCESS_TOKEN,
        },
        success: function(playlists) {
            let playlist;

            playlists.items.forEach(function(item) {
                if(item.name == playlistName) {
                    playlist = item;
                }
            });

            if(playlist===undefined) {
                // Create the Music Quiz playlist if it does not exist yet
                let data = {
                    'name': playlistName,
                    'public': playlistPublic,
                    'collaborative': playlistCollaborative,
                };

                data = JSON.stringify(data);

                $.ajax({
                    url: 'https://api.spotify.com/v1/users/' + USER_DATA.id + '/playlists',
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    headers: {
                    'Authorization': 'Bearer ' + ACCESS_TOKEN,
                    'Content-Type': 'application/json',
                    },
                    data: data,
                    success: function(playlist) {
                        console.log('playlist created');
                        callback(playlist);
                    },
                    error: function() {
                        console.log('error creating the playlist');
                    },
                });
            } else {
                console.log('playlist exist already');
                callback(playlist);
            }
        },
    });
}

/**
 *
 * This function update the quiz main timer
 * @param {Numeric} milliseconds Milliseconds to add or substract from the timer
 *
 **/
function updateTimer(milliseconds) {
    let counter = $('#counter');
    let myTime = counter.html();
    let ss = myTime.split(':');
    let dt = new Date();
    dt.setHours(ss[0]);
    dt.setMinutes(ss[1]);
    dt.setSeconds(ss[2]);

    let dt2 = new Date(dt.valueOf() + milliseconds);
    let ts = dt2.toTimeString().split(' ')[0];
    counter.html(ts);

    // If the time is over, we call the function 'endQuiz()'
    if(dt2.getHours() == 0 && dt2.getMinutes() == 0 && dt2.getSeconds() == 0) {
        endQuiz();
    }
}

/**
 *
 * This function setup and start the main timer of the quiz
 *
 **/
function countdown() {
    let counter = $('#counter');
    counter.html(quizDefaultDuration);
    counter.show();

    // Every second, we decrease the timer of 1s
    function update() {
        timer1 = setTimeout(update, 1000);
        updateTimer(-1000);
    }

    setTimeout(update, 1000);
}

/**
 *
 * Function which remove the tracks which appear more than once in the array
 * @param {Object[]} tracks Array of tracks to be played
 * @return {Object[]} Tracks without duplicate
 *
 **/
function removeTracksDuplicates(tracks) {
    let tracksWithoutDuplicates = [];

    for(let i = 0; i < tracks.length; i++) {
        let exist = false;
        let previewUrl = tracks[i].getPreviewUrl();
        for(let j = 0; j < tracksWithoutDuplicates.length; j++) {
            if(tracksWithoutDuplicates[j].getPreviewUrl() == previewUrl) {
                exist = true;
                break;
            }
        }

        if(!exist) {
            tracksWithoutDuplicates.push(tracks[i]);
        }
    }
    return tracksWithoutDuplicates;
}

/**
 *
 * If the track is not available (country restrictions, etc.), we remove it
 * @param {Object[]} tracks Array of tracks to be played
 * @return {Object[]} Tracks after removing restricted tracks
 *
 **/
function removeRestrictedTracks(tracks) {
    for(let i = 0; i < tracks.length; i++) {
        let track = tracks[i];
        if(track.getPreviewUrl()==null) {
            tracks.splice(i, 1);
        }
    }

    return tracks;
}

/**
 *
 * Function called on 'loadTracksFromPlaylist' and 'loadTracksByGenre' ajax success
 * @param {Object} data JSON data returned by Spotify Web API
 *
 **/
function loadTracksSuccess(data) {
    if(genre=='playlist') {
        data.items.forEach(function(track) {
            tracks.push(new Track(track.track));
        });
    } else {
        data.tracks.items.forEach(function(track) {
            tracks.push(new Track(track));
        });
    }

    // When all the tracks are loaded, we shuffle them and load the user instructions
    if(tracks.length == limit && genre != 'playlist') {
        tracksReady();
    } else if(genre == 'playlist' && tracks.length == data.total) {
        tracksReady();
    } else if(genre == 'playlist' && tracks.length < data.total) {
        let offset = data.offset + 100;

        let queryUrl = data.href;
        let regExp = /\/users\/(.*?)\/playlists\//;
        let ownerId = queryUrl.match(regExp)[1];
        regExp = /\/playlists\/(.*?)\/tracks/;
        let playlistId = queryUrl.match(regExp)[1];

        loadTracksFromPlaylist(playlistId, ownerId, offset);
    }

    function tracksReady() {
        tracks = shuffleArray(tracks);

        tracks = removeTracksDuplicates(tracks);
        tracks = removeRestrictedTracks(tracks);

        nextTrack = tracks[nextTrackID];
        nextTrack.preload();

        loadTemplate('game-instructions', function() {
            $('.gameInstructions button').click(function() {
                startQuiz();
            });
        });
    }
}

/**
 *
 * Load tracks by genre from Spotify API
 * @param {String} genre Genre from which the tracks have to be loaded
 *
 **/
function loadTracksByGenre(genre) {
    // Empty tracks array
    tracks = [];

    let data = {
        type: 'track',
        limit: 50,
        market: market,
    };

    if(genre=='70s') {
        data.q = 'year:1960-1970';
    } else if(genre=='80s') {
        data.q = 'year:1970-1980';
    } else {
        data.q = 'genre:'+genre;
    }

    // As we cannot get more than 50 tracks per search, we have to execute several query to get the expected number of tracks
    for(let offset = 0; offset<limit; offset+=50) {
        data.offset = offset;

        $.ajax({
            url: 'https://api.spotify.com/v1/search',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            data: data,
            success: loadTracksSuccess,
        });
    }
}

/**
 *
 * Load tracks from a user's playlist
 * @param {String} playlistId ID of the playlist
 * @param {String} playlistOwner Owner of the playlist
 * @param {Numeric} offset Offset for the query (Optional - default value : 0)
 *
 **/
function loadTracksFromPlaylist(playlistId, playlistOwner, offset) {
    offset = offset || 0;

    $.ajax({
        url: 'https://api.spotify.com/v1/users/' + playlistOwner + '/playlists/'+ playlistId + '/tracks',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
           'Authorization': 'Bearer ' + ACCESS_TOKEN,
        },
        data: {
            limit: 100, // 100 is the maximum value
            offset: offset,
        },
        success: loadTracksSuccess,
    });
}

/**
 *
 * Function called when the user select a genre or playlist
 *
 **/
function confirmGenre() {
    if($('input[name=genre]:checked', '#genre-selection').val() === undefined) {
        alert('You have to select a playlist first');
        return;
    }

    let attr = $('input[name=genre]:checked', '#genre-selection').attr('owner');

    // For some browsers, `attr` is undefined; for others,
    // `attr` is false.  Check for both.
    if (typeof attr !== typeof undefined && attr !== false) {
        let playlistId = $('input[name=genre]:checked', '#genre-selection').val();
        let playlistOwner = $('input[name=genre]:checked', '#genre-selection').attr('owner');
        genre = 'playlist';

        // Empty tracks array
        tracks = [];
        loadTracksFromPlaylist(playlistId, playlistOwner);
    } else {
        genre = $('input[name=genre]:checked', '#genre-selection').val();
        loadTracksByGenre(genre);
    }
}

$(document).ready(function() {
    // When the document is ready, load the genre selection template
    loadTemplate('genres', function() {
        $('#genre-selection').submit(function(event) {
            event.preventDefault();
            confirmGenre();
        });

        // Once the template loaded, check if the user is logged in. If yes, load the user playlists
        checkCookie('accessToken', function(accessToken) {
            ACCESS_TOKEN = accessToken;
            getUserPlaylists(accessToken)
                .then(function(response) {
                    showUserPlaylists(response);
                });
        });
    });

    // Check if the user is logged in. If yes, show the user profile button
    checkCookie('user', function(user) {
        USER_DATA = user;
        showUserProfile(user);
    });
});
