/**
 * FILE functions.js
 **/

/**
 *
 * Function which load a template
 * @param {String} templateName Name of the template to load
 * @param {function()} callback Callback function which will be called after loading the template
 * @param {Object} data Data to be compiled in the template
 *
 **/
function loadTemplate(templateName, callback, data) {
    data = data || {};

    let location = window.location.href;
    let directoryUrl = location.substring(0, location.lastIndexOf('/')+1);
    let templateUrl = directoryUrl+'templates/'+templateName+'.hbs';
    let page404Url = directoryUrl+'templates/404.hbs';

    $.ajax({
        url: templateUrl,
        type: 'HEAD',
        error: function() {
            // File not exists
            // 404 page

            $.get(page404Url, function(source) {
                let template=Handlebars.compile(source);

                let result = template(data);

                $('#wrapper').html(result);
            }, 'html');
        }, success: function() {
            // File exists
            // Load the template

            $.get(templateUrl, function(source) {
                let template=Handlebars.compile(source);

                let result = template(data);

                $('#wrapper').html(result);

                callback();
            }, 'html');
        },
    });
}

/**
 *
 * Shuffle the array given as a parameter
 * @param {Object[]} array Array to shuffle
 * @return {Object[]} Array shuffled
 *
 **/
function shuffleArray(array) {
    let currentIndex = array.length;
    let temporaryValue;
    let randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}
