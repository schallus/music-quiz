/**
 * FILE oauth.js
 **/

/**
 * GLOBAL VARIABLES
 **/

let ACCESS_TOKEN;
let USER_DATA;

/**
 * OAuth login request
 * @param {String} callback Callback function executed once we received the access_token.
 **/
function login(callback) {
	const CLIENT_ID = '6add326a80504a199a195132b566a766';
	const REDIRECT_URI = 'http://users.metropolia.fi/~floriasc/music-quiz/oauth.html';

	/**
     *
     * Create the login URL with the scopes given in as a parameter
     * @param {String[]} scopes Scopes necessary for the app.
     * @return {String} Login URL
     *
     **/
	function getLoginURL(scopes) {
		return 'https://accounts.spotify.com/authorize?client_id=' + CLIENT_ID +
            '&redirect_uri=' + encodeURIComponent(REDIRECT_URI) +
            '&scope=' + encodeURIComponent(scopes.join(' ')) +
            '&response_type=token';
	}

	let url = getLoginURL([
		'playlist-modify-private',
		'playlist-modify-public',
		'user-top-read',
		'user-read-email',
	]);

    // Event listener for POST message
	$(window).on('message onmessage', function(e) {
        let hash = JSON.parse(e.originalEvent.data);
        // If we received the access token, we call the callback function
        if (hash.type == 'access_token') {
			callback(hash.access_token);
		}
    });

    // Pop up window parameter
	let width = 450;
	let height = 730;
	let left = (screen.width / 2) - (width / 2);
	let top = (screen.height / 2) - (height / 2);

	// Open the Spotify connection window
	window.open(
        url,
        'Spotify',
        'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left
    );
}

/**
 *
 * Function which retrieves the user data
 * @param {String} accessToken User access_token needed for the query.
 * @return {Object} Object containing user data
 *
 **/
function getUserData(accessToken) {
    return $.ajax({
        url: 'https://api.spotify.com/v1/me',
        headers: {
           'Authorization': 'Bearer ' + accessToken,
        },
    });
}

/**
 *
 * Function which retrieves the playlists of the user
 * @param {String} accessToken User access_token needed for the query.
 * @return {Object} Object containing all the user playlists
 *
 **/
function getUserPlaylists(accessToken) {
    return $.ajax({
        url: 'https://api.spotify.com/v1/me/playlists',
        headers: {
           'Authorization': 'Bearer ' + accessToken,
        },
    });
}

function logout() {
    console.log('logout');
    setCookie('accessToken', null, 0);
    setCookie('user', null, 0);
    document.location.reload(true);
}

$(document).ready(function() {
    $('#spotifyAuthentication').click(function() {
        login(function(accessToken) {
            // Store the access token in your cookies
            setCookie('accessToken', accessToken, 3600);
            ACCESS_TOKEN = accessToken;

            // Get the user data
            getUserData(accessToken)
                .then(function(response) {
                    USER_DATA = response;
                    setCookie('user', response, 3600);
                    showUserProfile(response);
                });

            getUserPlaylists(accessToken)
                .then(function(response) {
                    showUserPlaylists(response);
                });
        });
    });
});
