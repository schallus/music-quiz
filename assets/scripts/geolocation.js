/**
 * FILE geolocation.js
 **/

/**
 *
 * Check is the client browser is compatible with Geolocation API
 * @param {function(coords)} callback Function called after getting the current position
 *
 **/
function getCurrentLocation(callback) {
    navigator.geolocation.getCurrentPosition(function(position) {
        callback(position.coords);
    },
    function(failure) {
        $.getJSON('https://ipinfo.io/geo', function(response) {
            let loc = response.loc.split(',');
            let coords = {
                latitude: loc[0],
                longitude: loc[1],
            };
            callback(coords);
        });
    });
}
