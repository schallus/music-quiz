/**
 * FILE gamejolt.js
 **/

/**
 * GLOBAL VARIABLES
 **/

 const gameId = '239311';

/**
 *
 * Save score using Game Jolt API
 * @param {String} userName Name of the user
 * @param {Numeric} score Score to save
 * @param {String} genre Genre of playlist
 *
 **/
function saveScore(userName, score, genre) {
    let data = {
        date: new Date(),
        genre: genre,
    };

    $.ajax({
        url: 'http://gamejolt.com/api/game/v1/scores/add/',
        type: 'GET',
        dataType: 'json',
        data: {
            game_id: gameId, // The ID of your game.
            score: score, // This is a string value associated with the score. Example: "234 Jumps".
            sort: score, // This is a numerical sorting value associated with the score. All sorting will work off of this number. Example: "234".
            guest: userName, // The guest's name. Leave blank if you're storing for a user.
            extra_data: JSON.stringify(data), // If there's any extra data you would like to store (as a string), you can use this variable. Note that this is only retrievable through the API. It never shows on the actual site.
        }, /*
        success: function() {
            console.log('score saved with success');
        },
        error: function() {
            console.log('error saving the score');
        }, */
    });
}

/**
 *
 * Returns a list of high score tables for a game
 * @param {String} gameId ID of the game to get the score from
 * @return {Object[]} High score table
 *
 **/
function getHighScoreTable(gameId) {
    return $.ajax({
        url: 'http://gamejolt.com/api/game/v1/scores/tables/',
        type: 'GET',
        dataType: 'json',
        data: {
            game_id: gameId, // The ID of your game.
        }, /*
        success: function(data) {
            console.log('success loading high score table');
            console.log(data);
        },
        error: function() {
            console.log('error loading high score table');
        }, */
    });
}
