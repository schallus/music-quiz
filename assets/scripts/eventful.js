/**
 * FILE eventful.js
 **/

const eventfulAppKey = 'w7TJNSGwt59H7rBf';

/**
 *
 * Search for events around the location given as a parameter
 * @param {Object} location Object containing a geo location
 * @param {function(coords)} callback Function called after getting the events
 *
 **/
function eventSearch(location, callback) {
    $.ajax({
        url: 'http://api.eventful.com/json/events/search',
        type: 'GET',
        dataType: 'jsonp',
        data: {
            app_key: eventfulAppKey,
            location: location.latitude + ', ' + location.longitude,
            within: 100, // Radius in km
            units: 'km',
            date: 'Future',
            category: 'music',
            sort_order: 'date',
        },
        success: function(data) {
            callback(data);
        },
        error: function(httpReq, status, exception) {
            console.log(status + ' ' + exception);
        },
    });
}
