/**
 * FILE cookies.js
 **/

/* global $ Handlebars */

'use strict';

/**
 *
 * Function which create a cookie
 * @param {String} cName Cookie name
 * @param {String|Object} cValue Cookie value (string or json)
 * @param {Numeric} exSeconds Expiration time in seconds
 *
 **/
function setCookie(cName, cValue, exSeconds) {
    if(cValue !== null && typeof cValue === 'object') {
        cValue = JSON.stringify(cValue);
    }

    let d = new Date();
    d.setTime(d.getTime() + (exSeconds*1000));
    let expires = 'expires='+ d.toUTCString();
    document.cookie = cName + '=' + cValue + ';' + expires + ';path=/';
}

/**
 *
 * Function which get a cookie
 * @param {String} cName Cookie name
 * @return {String|Object} Value of the cookie
 *
 **/
function getCookie(cName) {
    let name = cName + '=';
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            let cValue = c.substring(name.length, c.length);

            try {
                let json = JSON.parse(cValue);
                return json;
            } catch(e) {
                return cValue;
            }
        }
    }
    return '';
}

/**
 *
 * Function which check if a cookie exists. If yes, it calls the callback function
 * @param {String} cName Cookie name
 * @param {function(String)} callback
 *
 **/
function checkCookie(cName, callback) {
    let cookie = getCookie(cName);
    if (cookie != '') {
        callback(cookie);
    }
}
