/**
 * FILE handlebars.js
 **/

Handlebars.registerHelper('bold', function(options) {
    return new Handlebars.SafeString(
        '<strong>'
        + options.fn(this)
        + '</strong>');
});

Handlebars.registerHelper('formatDate', function(options) {
    let date = new Date(options.fn(this));
    return date.toDateString();
});
