/**
 * FILE track.js
 **/

/**
 *
 * Track class
 *
 **/

function Track(track) {
    this.id = track.id;
    this.name = track.name;
    this.artistName = track.artists[0].name;
    this.artistId = track.artists[0].id;
    this.album = track.album.name;
    this.uri = track.uri;

    if(track.album.images.length > 1) {
        this.albumCover = track.album.images[1].url;
    } else {
        this.albumCover = null;
    }

    this.previewUrl = track.preview_url || null;
    this.audio;

    this.getId = function() {
        return this.id;
    };

    this.getName = function() {
        return this.name;
    };

    this.getArtist = function() {
        return this.artistName;
    };

    this.getArtistId = function() {
        return this.artistId;
    };

    this.getAlbum = function() {
        return this.album;
    };

     this.getUri = function() {
        return this.uri;
    };

    this.getAlbumCover = function() {
        return this.albumCover;
    };

    this.getPreviewUrl = function() {
        return this.previewUrl;
    };

    this.preload = function() {
        this.audio = new Audio(this.previewUrl);
        this.audio.load();
    };

    this.play = function() {
        if (this.audio == null) {
            this.preload();
        } else {
            this.audio.play();
        }
    };

    this.stop = function() {
        this.audio.pause();
        this.audio.currentTime = 0;
    };
}
