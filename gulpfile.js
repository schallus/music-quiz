var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    notify = require('gulp-notify'),
    del = require('del'),
    concat = require('gulp-concat');

console.log(sass);

/*
    Task which compile main.scss and minify the code
*/
gulp.task('styles', function() {
  return sass('assets/sass/main.scss', { style: 'expanded' })
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest('css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('css'))
    .pipe(notify({ message: 'Styles task complete' }));
});

//script paths
var jsFiles = [
        'assets/scripts/cookies.js/',
        'assets/scripts/oauth.js/',
        'assets/scripts/functions.js/',
        'assets/scripts/geolocation.js/',
        'assets/scripts/eventful.js/',
        'assets/scripts/handlebars.js/',
        'assets/scripts/track.js/',
        'assets/scripts/main.js/',
    ],
    jsDest = 'scripts';

/*
    Task which concat all my scripts and minify the code
*/
gulp.task('scripts', function() {  
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDest));
});

/*
    Task which clean the CSS before to regenerate them
*/
gulp.task('clean', function() {
    return del(['public/css/*']);
});


/*
    Default task, ran by using $ gulp, to run all three tasks we have created
*/
gulp.task('default', ['clean'], function() {
    gulp.start('styles');
});

/* Watch */
gulp.task('watch', function() {
  /* Watch .scss files */
  gulp.watch('assets/sass/**/*.scss', ['styles']);
  gulp.watch('assets/scripts/**/*.js', ['scripts']);
});